package com.example.demo.controller;

import com.example.demo.dto.TaskDTO;
import com.example.demo.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("${app.endpoint.api}/task")
public class TaskController {
    final Logger logger = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private TaskService taskService;

    @PostMapping("/save")
    public boolean saveTask(@RequestBody TaskDTO taskDTO) {
        try {
            taskService.saveTask(taskDTO);
            return true;
        } catch (Exception e) {
            logger.error("Error when save task data  : " +e);
            e.printStackTrace();
            return false;
        }
    }

    @PostMapping("/update")
    public boolean updateTask(@RequestBody TaskDTO taskDTO) {
        try {
            taskService.updateTask(taskDTO);
            return true;
        } catch (Exception e) {
            logger.error("Error when update task data  : " +e);
            e.printStackTrace();
            return false;
        }
    }


    @GetMapping("/")
    public List<TaskDTO> getAllTasks() {
        try {
            return taskService.getAllTasks();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error in getting task Details: ", e);
            return null;
        }
    }

    @DeleteMapping("/delete/{id}")
    public boolean deleteTask(@PathVariable Integer id) {
        try {
            return taskService.deleteTask(id);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error in deleting task : ", e);
            return false;
        }

    }

}
