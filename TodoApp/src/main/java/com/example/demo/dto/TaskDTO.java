package com.example.demo.dto;

import lombok.Data;

import java.util.Date;

@Data
public class TaskDTO {
    private int id;
    private String title;
    private String description;
    private String status;
    private Date createDate;
    private Date endDate;
}
