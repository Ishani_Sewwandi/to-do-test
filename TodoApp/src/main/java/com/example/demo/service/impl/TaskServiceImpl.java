package com.example.demo.service.impl;

import com.example.demo.dto.TaskDTO;
import com.example.demo.entity.Task;
import com.example.demo.repository.TaskRepository;
import com.example.demo.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@Transactional
public class TaskServiceImpl implements TaskService {
    @Autowired
    private TaskRepository taskRepository;

    @Override
    public boolean saveTask(TaskDTO taskDTO) throws Exception {
        /** Check taskDTO is null or not **/
        if(taskDTO != null) {
            Task task = new Task();
            task.setTitle(taskDTO.getTitle());
            task.setDescription(taskDTO.getDescription());
            task.setStatus(taskDTO.getStatus());
            task.setCreatedDate(taskDTO.getCreateDate());
            task.setEndDate(taskDTO.getEndDate());

            /** Save data in repo **/
            taskRepository.save(task);
            return true;
        }else {
            return false;
        }
    }

    @Override
    public boolean updateTask(TaskDTO taskDTO) throws Exception {
        /** Check taskDTO is null or not **/
        if(taskDTO != null) {
            Task task = taskRepository.findById(taskDTO.getId()).get();
            task.setId(taskDTO.getId());
            task.setTitle(taskDTO.getTitle());
            task.setDescription(taskDTO.getDescription());
            task.setStatus(taskDTO.getStatus());
            task.setCreatedDate(taskDTO.getCreateDate());
            task.setEndDate(taskDTO.getEndDate());

            /** Save data in repo **/
            taskRepository.save(task);
            return true;
        }else {
            return false;
        }
    }

    @Override
    public List<TaskDTO> getAllTasks() throws Exception {
        /** Get All Task Details **/
        List<Task> all = taskRepository.findAll();
        List<TaskDTO> taskDTOS = new ArrayList<>();

        for (Task task:all) {
            TaskDTO taskDTO = new TaskDTO();
            taskDTO.setId(task.getId());
            taskDTO.setTitle(task.getTitle());
            taskDTO.setDescription(task.getDescription());
            taskDTO.setStatus(task.getStatus());
//            taskDTO.setCreatedDate(task.getCreateDate());
            taskDTO.setEndDate(task.getEndDate());

            taskDTOS.add(taskDTO);
        }
        return taskDTOS;
    }

    @Override
    public boolean deleteTask(int id) throws Exception {
       taskRepository.deleteById(id);
       return true;
    }
}
