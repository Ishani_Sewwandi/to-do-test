package com.example.demo.service;

import com.example.demo.dto.TaskDTO;

import java.util.List;

public interface TaskService {
    public boolean saveTask(TaskDTO taskDTO) throws Exception;

    public boolean updateTask(TaskDTO taskDTO)throws Exception;

    public List<TaskDTO> getAllTasks()throws Exception;

    public boolean deleteTask(int id)throws Exception;
}
